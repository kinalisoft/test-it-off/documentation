{
  "openapi": "3.0.1",
  "info": {
    "title": "Scene Web API Reference",
    "description": "",
    "version": "0.4.0"
  },
  "paths": {
    "/collisions": {
      "get": {
        "tags": [
          "Collisions"
        ],
        "summary": "Gets collision ids.",
        "operationId": "GetCollisionsIds",
        "responses": {
          "200": {
            "description": "Collisions successfully retrieved.",
            "content": {
              "text/plain": {
                "schema": {
                  "type": "array",
                  "items": {
                    "type": "string"
                  }
                }
              },
              "application/json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "type": "string"
                  }
                }
              },
              "text/json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "type": "string"
                  }
                }
              }
            }
          },
          "500": {
            "description": "Service error caused by application state violation.",
            "content": {
              "text/plain": {
                "schema": {
                  "type": "string"
                }
              },
              "application/json": {
                "schema": {
                  "type": "string"
                }
              },
              "text/json": {
                "schema": {
                  "type": "string"
                }
              }
            }
          }
        }
      }
    },
    "/collisions/box": {
      "put": {
        "tags": [
          "Collisions"
        ],
        "summary": "Puts collisions box.",
        "operationId": "SetCollisionBox",
        "parameters": [
          {
            "name": "boxId",
            "in": "query",
            "description": "Unique system id.",
            "required": true,
            "schema": {
              "type": "string",
              "description": "Unique system id."
            }
          },
          {
            "name": "sizeX",
            "in": "query",
            "description": "Size of box in X coordinate.",
            "required": true,
            "schema": {
              "minimum": 0,
              "type": "number",
              "description": "Size of box in X coordinate.",
              "format": "float"
            }
          },
          {
            "name": "sizeY",
            "in": "query",
            "description": "Size of box in Y coordinate.",
            "required": true,
            "schema": {
              "minimum": 0,
              "type": "number",
              "description": "Size of box in Y coordinate.",
              "format": "float"
            }
          },
          {
            "name": "sizeZ",
            "in": "query",
            "description": "Size of box in Z coordinate.",
            "required": true,
            "schema": {
              "minimum": 0,
              "type": "number",
              "description": "Size of box in Z coordinate.",
              "format": "float"
            }
          },
          {
            "name": "transformId",
            "in": "query",
            "description": "Id of the transformation to which the pose is related.",
            "schema": {
              "type": "string",
              "description": "Id of the transformation to which the pose is related.",
              "nullable": true
            }
          }
        ],
        "requestBody": {
          "description": "Relative pose in space defined by tranformation id.",
          "content": {
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/Pose"
              }
            },
            "text/json": {
              "schema": {
                "$ref": "#/components/schemas/Pose"
              }
            },
            "application/*+json": {
              "schema": {
                "$ref": "#/components/schemas/Pose"
              }
            }
          },
          "required": true
        },
        "responses": {
          "200": {
            "description": "Collision box successfully set."
          },
          "500": {
            "description": "Service error caused by application state violation.",
            "content": {
              "text/plain": {
                "schema": {
                  "type": "string"
                }
              },
              "application/json": {
                "schema": {
                  "type": "string"
                }
              },
              "text/json": {
                "schema": {
                  "type": "string"
                }
              }
            }
          }
        }
      }
    },
    "/collisions/sphere": {
      "put": {
        "tags": [
          "Collisions"
        ],
        "summary": "Puts collisions sphere.",
        "operationId": "SetCollisionSphere",
        "parameters": [
          {
            "name": "sphereId",
            "in": "query",
            "description": "Unique system id.",
            "required": true,
            "schema": {
              "type": "string",
              "description": "Unique system id."
            }
          },
          {
            "name": "radius",
            "in": "query",
            "description": "Size of sphere radius.",
            "required": true,
            "schema": {
              "minimum": 0,
              "type": "number",
              "description": "Size of sphere radius.",
              "format": "float"
            }
          },
          {
            "name": "transformId",
            "in": "query",
            "description": "Id of the transformation to which the pose is related.",
            "schema": {
              "type": "string",
              "description": "Id of the transformation to which the pose is related.",
              "nullable": true
            }
          }
        ],
        "requestBody": {
          "description": "Relative pose in space defined by tranformation id.",
          "content": {
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/Pose"
              }
            },
            "text/json": {
              "schema": {
                "$ref": "#/components/schemas/Pose"
              }
            },
            "application/*+json": {
              "schema": {
                "$ref": "#/components/schemas/Pose"
              }
            }
          },
          "required": true
        },
        "responses": {
          "200": {
            "description": "Collision sphere successfully set."
          },
          "500": {
            "description": "Service error caused by application state violation.",
            "content": {
              "text/plain": {
                "schema": {
                  "type": "string"
                }
              },
              "application/json": {
                "schema": {
                  "type": "string"
                }
              },
              "text/json": {
                "schema": {
                  "type": "string"
                }
              }
            }
          }
        }
      }
    },
    "/collisions/cylinder": {
      "put": {
        "tags": [
          "Collisions"
        ],
        "summary": "Puts collisions cylinder.",
        "operationId": "SetCollisionCylinder",
        "parameters": [
          {
            "name": "cylinderId",
            "in": "query",
            "description": "Unique collision id.",
            "required": true,
            "schema": {
              "type": "string",
              "description": "Unique collision id."
            }
          },
          {
            "name": "radius",
            "in": "query",
            "description": "Size of cylinder radius.",
            "required": true,
            "schema": {
              "minimum": 0,
              "type": "number",
              "description": "Size of cylinder radius.",
              "format": "float"
            }
          },
          {
            "name": "height",
            "in": "query",
            "description": "Size of cylinder height.",
            "required": true,
            "schema": {
              "minimum": 0,
              "type": "number",
              "description": "Size of cylinder height.",
              "format": "float"
            }
          },
          {
            "name": "transformId",
            "in": "query",
            "description": "Id of the transformation to which the pose is related.",
            "schema": {
              "type": "string",
              "description": "Id of the transformation to which the pose is related.",
              "nullable": true
            }
          }
        ],
        "requestBody": {
          "description": "Relative pose in space defined by tranformation id.",
          "content": {
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/Pose"
              }
            },
            "text/json": {
              "schema": {
                "$ref": "#/components/schemas/Pose"
              }
            },
            "application/*+json": {
              "schema": {
                "$ref": "#/components/schemas/Pose"
              }
            }
          },
          "required": true
        },
        "responses": {
          "200": {
            "description": "Collision cylinder successfully set."
          },
          "500": {
            "description": "Service error caused by application state violation.",
            "content": {
              "text/plain": {
                "schema": {
                  "type": "string"
                }
              },
              "application/json": {
                "schema": {
                  "type": "string"
                }
              },
              "text/json": {
                "schema": {
                  "type": "string"
                }
              }
            }
          }
        }
      }
    },
    "/collisions/mesh": {
      "put": {
        "tags": [
          "Collisions"
        ],
        "summary": "Puts collisions mesh.",
        "operationId": "SetCollisionMesh",
        "parameters": [
          {
            "name": "meshId",
            "in": "query",
            "description": "Unique system id.",
            "required": true,
            "schema": {
              "type": "string",
              "description": "Unique system id."
            }
          },
          {
            "name": "uri",
            "in": "query",
            "description": "Uniform resource identification of STL model.",
            "required": true,
            "schema": {
              "type": "string",
              "description": "Uniform resource identification of STL model."
            }
          },
          {
            "name": "meshScaleX",
            "in": "query",
            "description": "Collision mesh scale for X axis",
            "schema": {
              "minimum": 0,
              "type": "number",
              "description": "Collision mesh scale for X axis",
              "format": "float",
              "default": 1
            }
          },
          {
            "name": "meshScaleY",
            "in": "query",
            "description": "Collision mesh scale for Y axis",
            "schema": {
              "minimum": 0,
              "type": "number",
              "description": "Collision mesh scale for Y axis",
              "format": "float",
              "default": 1
            }
          },
          {
            "name": "meshScaleZ",
            "in": "query",
            "description": "Collision mesh scale for Z axis",
            "schema": {
              "minimum": 0,
              "type": "number",
              "description": "Collision mesh scale for Z axis",
              "format": "float",
              "default": 1
            }
          },
          {
            "name": "transformId",
            "in": "query",
            "description": "Id of the transformation to which the pose is related.\r\n            If not provided, default root transform id is used.",
            "schema": {
              "type": "string",
              "description": "Id of the transformation to which the pose is related.\r\n            If not provided, default root transform id is used.",
              "nullable": true
            }
          }
        ],
        "requestBody": {
          "description": "Relative pose in space defined by tranformationId.",
          "content": {
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/Pose"
              }
            },
            "text/json": {
              "schema": {
                "$ref": "#/components/schemas/Pose"
              }
            },
            "application/*+json": {
              "schema": {
                "$ref": "#/components/schemas/Pose"
              }
            }
          },
          "required": true
        },
        "responses": {
          "200": {
            "description": "Collision mesh successfully set."
          },
          "500": {
            "description": "Service error caused by application state violation.",
            "content": {
              "text/plain": {
                "schema": {
                  "type": "string"
                }
              },
              "application/json": {
                "schema": {
                  "type": "string"
                }
              },
              "text/json": {
                "schema": {
                  "type": "string"
                }
              }
            }
          }
        }
      }
    },
    "/collisions/{collisionId}": {
      "delete": {
        "tags": [
          "Collisions"
        ],
        "summary": "Deletes collision.",
        "operationId": "DeleteCollision",
        "parameters": [
          {
            "name": "collisionId",
            "in": "path",
            "description": "Unique collision id.",
            "required": true,
            "schema": {
              "type": "string",
              "description": "Unique collision id.",
              "nullable": true
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Collision successfully deleted."
          },
          "500": {
            "description": "Service error caused by application state violation.",
            "content": {
              "text/plain": {
                "schema": {
                  "type": "string"
                }
              },
              "application/json": {
                "schema": {
                  "type": "string"
                }
              },
              "text/json": {
                "schema": {
                  "type": "string"
                }
              }
            }
          }
        }
      }
    },
    "/system/start": {
      "put": {
        "tags": [
          "System"
        ],
        "summary": "Creates the system.",
        "operationId": "Start",
        "responses": {
          "200": {
            "description": "System successfully started."
          },
          "500": {
            "description": "Service error caused by application state violation.",
            "content": {
              "text/plain": {
                "schema": {
                  "type": "string"
                }
              },
              "application/json": {
                "schema": {
                  "type": "string"
                }
              },
              "text/json": {
                "schema": {
                  "type": "string"
                }
              }
            }
          }
        }
      }
    },
    "/system/stop": {
      "put": {
        "tags": [
          "System"
        ],
        "summary": "Destroys the system (if exists).",
        "operationId": "Stop",
        "responses": {
          "200": {
            "description": "System successfully started."
          },
          "500": {
            "description": "Service error caused by application state violation.",
            "content": {
              "text/plain": {
                "schema": {
                  "type": "string"
                }
              },
              "application/json": {
                "schema": {
                  "type": "string"
                }
              },
              "text/json": {
                "schema": {
                  "type": "string"
                }
              }
            }
          }
        }
      }
    },
    "/system/running": {
      "get": {
        "tags": [
          "System"
        ],
        "summary": "Gets information if system is running.",
        "operationId": "IsRunning",
        "responses": {
          "200": {
            "description": "System state retrieved successfully.",
            "content": {
              "text/plain": {
                "schema": {
                  "type": "boolean"
                }
              },
              "application/json": {
                "schema": {
                  "type": "boolean"
                }
              },
              "text/json": {
                "schema": {
                  "type": "boolean"
                }
              }
            }
          },
          "500": {
            "description": "Server error caused by unexpected application behavior.",
            "content": {
              "text/plain": {
                "schema": {
                  "type": "string"
                }
              },
              "application/json": {
                "schema": {
                  "type": "string"
                }
              },
              "text/json": {
                "schema": {
                  "type": "string"
                }
              }
            }
          }
        }
      }
    },
    "/transforms": {
      "get": {
        "tags": [
          "Transforms"
        ],
        "summary": "Gets avalaible transformations.",
        "operationId": "GetTransforms",
        "responses": {
          "200": {
            "description": "Transforms successfully retrieved.",
            "content": {
              "text/plain": {
                "schema": {
                  "type": "array",
                  "items": {
                    "type": "string"
                  }
                }
              },
              "application/json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "type": "string"
                  }
                }
              },
              "text/json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "type": "string"
                  }
                }
              }
            }
          },
          "500": {
            "description": "Service error caused by application state violation.",
            "content": {
              "text/plain": {
                "schema": {
                  "type": "string"
                }
              },
              "application/json": {
                "schema": {
                  "type": "string"
                }
              },
              "text/json": {
                "schema": {
                  "type": "string"
                }
              }
            }
          }
        }
      },
      "put": {
        "tags": [
          "Transforms"
        ],
        "summary": "Adds or updates transform.",
        "operationId": "CreateTransform",
        "parameters": [
          {
            "name": "transformId",
            "in": "query",
            "description": "Unique transform id.",
            "schema": {
              "type": "string",
              "description": "Unique transform id.",
              "nullable": true
            }
          },
          {
            "name": "parent",
            "in": "query",
            "description": "Parent transform id.",
            "schema": {
              "type": "string",
              "description": "Parent transform id.",
              "nullable": true
            }
          }
        ],
        "requestBody": {
          "description": "Pose relation to parent frame.",
          "content": {
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/Pose"
              }
            },
            "text/json": {
              "schema": {
                "$ref": "#/components/schemas/Pose"
              }
            },
            "application/*+json": {
              "schema": {
                "$ref": "#/components/schemas/Pose"
              }
            }
          },
          "required": true
        },
        "responses": {
          "200": {
            "description": "Transform successfully created."
          },
          "500": {
            "description": "Service error caused by application state violation.",
            "content": {
              "text/plain": {
                "schema": {
                  "type": "string"
                }
              },
              "application/json": {
                "schema": {
                  "type": "string"
                }
              },
              "text/json": {
                "schema": {
                  "type": "string"
                }
              }
            }
          }
        }
      }
    },
    "/transforms/{transformId}/localPose": {
      "get": {
        "tags": [
          "Transforms"
        ],
        "summary": "Gets relative pose to parent.",
        "operationId": "GetLocalTransform",
        "parameters": [
          {
            "name": "transformId",
            "in": "path",
            "description": "Unique transform id.",
            "required": true,
            "schema": {
              "type": "string",
              "description": "Unique transform id.",
              "nullable": true
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Local transform successfully calculated.",
            "content": {
              "text/plain": {
                "schema": {
                  "$ref": "#/components/schemas/Pose"
                }
              },
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/Pose"
                }
              },
              "text/json": {
                "schema": {
                  "$ref": "#/components/schemas/Pose"
                }
              }
            }
          },
          "500": {
            "description": "Service error caused by application state violation.",
            "content": {
              "text/plain": {
                "schema": {
                  "type": "string"
                }
              },
              "application/json": {
                "schema": {
                  "type": "string"
                }
              },
              "text/json": {
                "schema": {
                  "type": "string"
                }
              }
            }
          }
        }
      }
    },
    "/transforms/{transformId}/worldPose": {
      "get": {
        "tags": [
          "Transforms"
        ],
        "summary": "Gets absolute pose in world space.",
        "operationId": "GetWorldTransform",
        "parameters": [
          {
            "name": "transformId",
            "in": "path",
            "description": "Unique transform id.",
            "required": true,
            "schema": {
              "type": "string",
              "description": "Unique transform id.",
              "nullable": true
            }
          }
        ],
        "responses": {
          "200": {
            "description": "World transform successfully calculated.",
            "content": {
              "text/plain": {
                "schema": {
                  "$ref": "#/components/schemas/Pose"
                }
              },
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/Pose"
                }
              },
              "text/json": {
                "schema": {
                  "$ref": "#/components/schemas/Pose"
                }
              }
            }
          },
          "500": {
            "description": "Service error caused by application state violation.",
            "content": {
              "text/plain": {
                "schema": {
                  "type": "string"
                }
              },
              "application/json": {
                "schema": {
                  "type": "string"
                }
              },
              "text/json": {
                "schema": {
                  "type": "string"
                }
              }
            }
          }
        }
      }
    },
    "/transforms/{transformId}": {
      "delete": {
        "tags": [
          "Transforms"
        ],
        "summary": "Deletes transformation matrix.",
        "operationId": "DeleteTransform",
        "parameters": [
          {
            "name": "transformId",
            "in": "path",
            "description": "Unique transform id.",
            "required": true,
            "schema": {
              "type": "string",
              "description": "Unique transform id.",
              "nullable": true
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Transform successfully deleted."
          },
          "500": {
            "description": "Service error caused by application state violation.",
            "content": {
              "text/plain": {
                "schema": {
                  "type": "string"
                }
              },
              "application/json": {
                "schema": {
                  "type": "string"
                }
              },
              "text/json": {
                "schema": {
                  "type": "string"
                }
              }
            }
          }
        }
      }
    },
    "/utils/focus": {
      "put": {
        "tags": [
          "Utils"
        ],
        "summary": "Calculates position of object.",
        "operationId": "FocusMesh",
        "requestBody": {
          "description": "Focus in and out points.",
          "content": {
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/Focus"
              }
            },
            "text/json": {
              "schema": {
                "$ref": "#/components/schemas/Focus"
              }
            },
            "application/*+json": {
              "schema": {
                "$ref": "#/components/schemas/Focus"
              }
            }
          },
          "required": true
        },
        "responses": {
          "200": {
            "description": "Mesh successfully focused.",
            "content": {
              "text/plain": {
                "schema": {
                  "$ref": "#/components/schemas/Pose"
                }
              },
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/Pose"
                }
              },
              "text/json": {
                "schema": {
                  "$ref": "#/components/schemas/Pose"
                }
              }
            }
          },
          "500": {
            "description": "Service error caused by application state violation.",
            "content": {
              "text/plain": {
                "schema": {
                  "type": "string"
                }
              },
              "application/json": {
                "schema": {
                  "type": "string"
                }
              },
              "text/json": {
                "schema": {
                  "type": "string"
                }
              }
            }
          }
        }
      }
    }
  },
  "components": {
    "schemas": {
      "Orientation": {
        "type": "object",
        "properties": {
          "w": {
            "type": "number",
            "format": "double"
          },
          "x": {
            "type": "number",
            "format": "double"
          },
          "y": {
            "type": "number",
            "format": "double"
          },
          "z": {
            "type": "number",
            "format": "double"
          }
        },
        "additionalProperties": false
      },
      "Position": {
        "type": "object",
        "properties": {
          "x": {
            "type": "number",
            "format": "double"
          },
          "y": {
            "type": "number",
            "format": "double"
          },
          "z": {
            "type": "number",
            "format": "double"
          }
        },
        "additionalProperties": false
      },
      "Pose": {
        "type": "object",
        "properties": {
          "orientation": {
            "$ref": "#/components/schemas/Orientation"
          },
          "position": {
            "$ref": "#/components/schemas/Position"
          }
        },
        "additionalProperties": false
      },
      "Focus": {
        "type": "object",
        "properties": {
          "meshFocusPoints": {
            "type": "array",
            "items": {
              "$ref": "#/components/schemas/Position"
            },
            "nullable": true
          },
          "robotSpacePoints": {
            "type": "array",
            "items": {
              "$ref": "#/components/schemas/Position"
            },
            "nullable": true
          }
        },
        "additionalProperties": false
      }
    }
  }
}