{
  "openapi": "3.0.1",
  "info": {
    "title": "Scanner Web API",
    "description": "Dependencies: Asset 1.0.0",
    "version": "1.0.0"
  },
  "paths": {
    "/scanner/scan": {
      "get": {
        "tags": [
          "Scanner"
        ],
        "summary": "Performs a scan and returns scanned code.",
        "operationId": "Scan",
        "responses": {
          "200": {
            "description": "Scanned code as a string.",
            "content": {
              "application/json": {
                "schema": {
                  "type": "string"
                }
              }
            }
          },
          "500": {
            "description": "Error Types: **NoActiveSystem**, **DeviceConnection**, **DeviceInitialization**",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/WebApiError"
                }
              }
            }
          }
        }
      }
    },
    "/system/set": {
      "put": {
        "tags": [
          "System"
        ],
        "summary": "Creates a system using the specified configuration.",
        "operationId": "SetSystem",
        "parameters": [
          {
            "name": "configurationId",
            "in": "query",
            "description": "The unique identification of configuration.",
            "required": true,
            "schema": {
              "type": "string"
            }
          },
          {
            "name": "id",
            "in": "query",
            "description": "The unique identification of the system (entity).\r\nThis parameter represents unique entity name and replaces entity name in configuration.",
            "required": true,
            "schema": {
              "type": "string"
            }
          }
        ],
        "requestBody": {
          "description": "The pose of the system.",
          "content": {
            "application/json": {
              "schema": {
                "allOf": [
                  {
                    "$ref": "#/components/schemas/Pose"
                  }
                ],
                "description": "Represents spatial 6D pose."
              }
            }
          },
          "required": true
        },
        "responses": {
          "200": {
            "description": "System successfully created."
          },
          "500": {
            "description": "Error Types: **SystemInitialization**, **SystemConfiguration**, **DeviceInitialization**, **DeviceConnection**",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/WebApiError"
                }
              }
            }
          }
        }
      }
    },
    "/system/reset": {
      "put": {
        "tags": [
          "System"
        ],
        "summary": "Destroys the active system (if exists).",
        "operationId": "ResetSystem",
        "responses": {
          "200": {
            "description": "System successfully destroyed."
          }
        }
      }
    },
    "/system/pose": {
      "get": {
        "tags": [
          "System"
        ],
        "summary": "Gets the pose of the system.",
        "operationId": "GetPose",
        "responses": {
          "200": {
            "description": "The pose of the active system or zero pose if the system is not active.",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/Pose"
                }
              }
            }
          }
        }
      }
    },
    "/system/configuration": {
      "get": {
        "tags": [
          "System"
        ],
        "summary": "Gets the ID of the system configuration.",
        "operationId": "GetConfigurationId",
        "responses": {
          "200": {
            "description": "The ID of active system configuration or an emtpy string if the system is not active.",
            "content": {
              "application/json": {
                "schema": {
                  "type": "string"
                }
              }
            }
          }
        }
      }
    },
    "/tracing": {
      "get": {
        "tags": [
          "Tracing"
        ],
        "summary": "Gets the current tracing level set for each trace target on the service.",
        "operationId": "GetTraceTargetsLevels",
        "responses": {
          "200": {
            "description": "Tracing level of each trace target successfully returned.",
            "content": {
              "application/json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "$ref": "#/components/schemas/TraceTargetSettings"
                  }
                }
              }
            }
          }
        }
      },
      "put": {
        "tags": [
          "Tracing"
        ],
        "summary": "Sets specific tracing level for each trace target on the service.",
        "description": "Unknown trace target ID is ignored.",
        "operationId": "SetTraceTargetsLevels",
        "requestBody": {
          "description": "Desired tracing level for each target.",
          "content": {
            "application/json": {
              "schema": {
                "type": "array",
                "items": {
                  "$ref": "#/components/schemas/TraceTargetSettings"
                }
              }
            }
          },
          "required": true
        },
        "responses": {
          "200": {
            "description": "New tracing level for each trace target successfully set."
          }
        }
      }
    },
    "/tracing/chrome": {
      "get": {
        "tags": [
          "Tracing"
        ],
        "summary": "Gets tracing events as .json text file in format for Chromium tracing viewer.",
        "operationId": "GetChromeTargetFile",
        "responses": {
          "200": {
            "description": "Chrome log file succesfully downloaded.",
            "content": {
              "application/octet-stream": {
                "schema": {
                  "type": "string",
                  "format": "binary"
                }
              }
            }
          }
        }
      },
      "delete": {
        "tags": [
          "Tracing"
        ],
        "summary": "Removes all the previously written Chromium tracing events.",
        "operationId": "ClearTraces",
        "responses": {
          "200": {
            "description": "The events were succesfully removed."
          }
        }
      }
    }
  },
  "components": {
    "schemas": {
      "Orientation": {
        "type": "object",
        "properties": {
          "x": {
            "type": "number",
            "description": "Gets or sets X rotation component.",
            "format": "double"
          },
          "y": {
            "type": "number",
            "description": "Gets or sets Y rotation component.",
            "format": "double"
          },
          "z": {
            "type": "number",
            "description": "Gets or sets Z rotation component.",
            "format": "double"
          },
          "w": {
            "type": "number",
            "description": "Gets or sets W rotation component.",
            "format": "double",
            "default": 1
          }
        },
        "additionalProperties": false,
        "description": "Represents model of orientation."
      },
      "Pose": {
        "type": "object",
        "properties": {
          "position": {
            "allOf": [
              {
                "$ref": "#/components/schemas/Position"
              }
            ],
            "description": "Gets position.",
            "nullable": true
          },
          "orientation": {
            "allOf": [
              {
                "$ref": "#/components/schemas/Orientation"
              }
            ],
            "description": "Gets rotation.",
            "nullable": true
          }
        },
        "additionalProperties": false,
        "description": "Represents spatial 6D pose."
      },
      "Position": {
        "type": "object",
        "properties": {
          "x": {
            "type": "number",
            "description": "Gets or sets X axis coordinate in meters.",
            "format": "double"
          },
          "y": {
            "type": "number",
            "description": "Gets or sets Y axis coordinate in meters.",
            "format": "double"
          },
          "z": {
            "type": "number",
            "description": "Gets or sets Z axis coordinate in meters.",
            "format": "double"
          }
        },
        "additionalProperties": false,
        "description": "Represents model of spatial position, stores values stored in meters."
      },
      "SystemErrorData": {
        "type": "object",
        "properties": {
          "rootEntityName": {
            "type": "string",
            "description": "Gets the name of the configuration's root entity if the system fails due to an\r\nerror in configuration (can be empty).",
            "nullable": true
          },
          "entityName": {
            "type": "string",
            "description": "Gets the name of the configuration's entity which leads to initialization failure\r\n(can be empty).",
            "nullable": true
          }
        },
        "additionalProperties": false,
        "description": "Data model for system related error."
      },
      "TraceTargetSettings": {
        "type": "object",
        "properties": {
          "traceTargetId": {
            "type": "string",
            "description": "Gets or initializes trace target name for which are settings.",
            "nullable": true
          },
          "tracingLevel": {
            "allOf": [
              {
                "$ref": "#/components/schemas/TracingLevel"
              }
            ],
            "description": "Levels for activity tracer."
          }
        },
        "additionalProperties": false,
        "description": "Settings for trace target."
      },
      "TracingLevel": {
        "enum": [
          "Trace",
          "Debug",
          "Info",
          "Warn",
          "Error",
          "Fatal",
          "Off",
          "All"
        ],
        "type": "string",
        "description": "Levels for activity tracer."
      },
      "WebApiError": {
        "required": [
          "description",
          "message",
          "service",
          "type"
        ],
        "type": "object",
        "properties": {
          "service": {
            "minLength": 1,
            "type": "string",
            "description": "Gets the service name where error occured."
          },
          "message": {
            "minLength": 1,
            "type": "string",
            "description": "Gets human-readable explanation specific to this occurrence of the problem."
          },
          "type": {
            "minLength": 1,
            "type": "string",
            "description": "Gets application specific error type.  \r\n\r\n **Error Types**  \r\n<pre> - Unexpected                | Occurs when there is unexpected internal server error.                                     | -                          \r\n - SystemInitialization      | Occurs when the system is not able to initialize due to general error.                     | SystemErrorData            \r\n - SystemConfiguration       | Occurs when the system configuration has wrong format or data are incorrect.               | SystemErrorData            \r\n - DeviceConnection          | Occurs when the system could not connect to underlying device.                             | SystemErrorData            \r\n - DeviceInitialization      | Occurs when the system cannot initialize or control underlying device.                     | SystemErrorData            \r\n - NoActiveSystem            | Occurs when there is an attempt to control system which is not active.                     | -                          \r\n - NotFound                  | Occurs when anything that was looked for is not found.                                     | -                          \r\n</pre>"
          },
          "description": {
            "minLength": 1,
            "type": "string",
            "description": "Gets short, human-readable description\r\nof the application specific Kinalisoft.Core.WebApi.WebApiError.Type."
          },
          "content": {
            "type": "string",
            "description": "Gets specific JSON object to this occurrence of the problem.\r\nThe data model is specific to the Kinalisoft.Core.WebApi.WebApiError.Type\r\nand should be documented on each API method.",
            "nullable": true
          }
        },
        "additionalProperties": false,
        "description": "Represents any web api error."
      }
    }
  }
}